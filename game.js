var ZI = (function (parent) {
	var my = parent;

    my.DEBUG = false;

	var WINDOW_WIDTH  = 800;
    var WINDOW_HEIGHT = 600;
	
	/* phaser init */
	my.game = new Phaser.Game(
        WINDOW_WIDTH, WINDOW_HEIGHT,
        Phaser.AUTO,
        'phaser-container'
    );
    
	return parent;
}(ZI || {}));

var ZI = (function (parent) {
	var my = parent.BootState = parent.BootState || {};
	var game = parent.game;

	my.preload = function () {
        // allow external assets
        //game.load.baseURL = 'https://codewiththepros.org/s3/';
        //game.load.crossOrigin = 'anonymous';
        
        game.load.image('title', 'phaser/hawthorne2016/Art/composed/title.jpg');
        game.load.image('preloadBar', 'phaser/demo/preload_bar.png');
	};
	
	my.create = function () {
        game.state.start('LoadState');
	};
	
	game.state.add('BootState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
	var my = parent.LoadState = parent.LoadState || {};
	var game = parent.game;

    my.preload = function() {
        // allow external assets
        //game.load.baseURL = 'https://codewiththepros.org/s3/';
        //game.load.crossOrigin = 'anonymous';
        game.stage.backgroundColor = '#ffffff';
        
        var logo = game.add.sprite(game.world.centerX, game.world.centerY - 128, 'title');
        logo.anchor.setTo(0.5, 0.5);
        
        var preloadBar = game.add.sprite(game.world.centerX, game.world.centerY + 64, 'preloadBar')
        preloadBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(preloadBar);
        
        // load assets
        game.load.image('storyButton', 'phaser/demo/storyButton.png');
        game.load.image('gameButton', 'phaser/demo/gameButton.png');
        game.load.image('restartButton', 'phaser/demo/restartButton.png');
        game.load.image('creditsButton', 'phaser/demo/creditsButton.png');
        game.load.image('menuButton', 'phaser/demo/menuButton.png');
        
        game.load.image('ground', 'phaser/demo/ground.png');
        game.load.image('building', 'phaser/demo/building.png');
        game.load.image('platform', 'phaser/demo/platform.png');
        game.load.image('ladder', 's3/pics/Ladder1.png');
        game.load.spritesheet('dude', 'phaser/demo/dude.png', 32, 48); // don't use
        game.load.spritesheet('baddie', 'phaser/demo/baddie.png', 32, 32); // don't use
       
        game.load.image('OneEyeGuy', 's3/pics/OneEyeGuy.png'); 
        game.load.image('FourEyeGuy', 's3/pics/FourEyeGuy.png'); 
        game.load.image('GreenGuy', 's3/pics/GreenGuy.png'); 
        game.load.image('BigBoss', 's3/pics/BigBoss.png'); 
        game.load.image('DoubleBoss', 's3/pics/DoubleBoss.png');

        game.load.image('PoisonTurret', 's3/pics/PoisonTurret.png');
        game.load.image('PoisonBullet', 's3/pics/PoisonBullet.png');
        game.load.image('Basketball', 's3/pics/BasketballGun.png');
        game.load.image('Fireball', 's3/pics/Fireball.png');
        game.load.image('CupcakeLit', 's3/pics/ShieldCupcakeLit.png');
        game.load.image('CupcakeOut', 's3/pics/ShieldCupcakeOut.png');
        game.load.image('Tank', 's3/pics/Tank.png'); 
        
        game.load.image('Dog', 's3/pics/Dog.png'); 
        game.load.spritesheet('player1', 's3/pics/Player1Sprites.png', 25, 45);       

        game.load.image('theMall', 'phaser/hawthorne2016/Art/composed/the_mall.jpg');
        game.load.image('fireBuildings', 'phaser/hawthorne2016/Art/composed/fire_buildings.jpg');
        game.load.image('title2', 'phaser/hawthorne2016/Art/composed/title2.jpg');
        game.load.image('title3', 'phaser/hawthorne2016/Art/composed/title3.jpg');
        
        // audio
        game.load.audio('Crunch', 'phaser/hawthorne2016/Audio/Crunch.mp3');
        game.load.audio('Ff', 'phaser/hawthorne2016/Audio/Ff.mp3');
        game.load.audio('Grrr', 'phaser/hawthorne2016/Audio/Grrr.mp3');
        game.load.audio('Grrr2', 'phaser/hawthorne2016/Audio/Grrr2.mp3');
        game.load.audio('HaDead', 'phaser/hawthorne2016/Audio/HaDead.mp3');
        game.load.audio('IntroDialog', 'phaser/hawthorne2016/Audio/IntroDialog.mp3');
        game.load.audio('Kablam', 'phaser/hawthorne2016/Audio/Kablam.mp3');
        game.load.audio('MoreDefenses', 'phaser/hawthorne2016/Audio/MoreDefenses.mp3');
        game.load.audio('PoofCha', 'phaser/hawthorne2016/Audio/PoofCha.mp3');
        game.load.audio('Pow', 'phaser/hawthorne2016/Audio/Pow.mp3');
        game.load.audio('Ppsh', 'phaser/hawthorne2016/Audio/Ppsh.mp3');
        game.load.audio('Run', 'phaser/hawthorne2016/Audio/Run!.mp3');
        
        game.load.image('Sword', 'phaser/hawthorne2016/Art/Sword.png');
        game.load.image('Sword3', 's3/pics/Sword3.png');

        game.load.script('gray', 'scripts/Gray.js');
        game.load.image('TreeBack', 's3/pics/TreeBack.png');
    };

	my.create = function () {
		game.state.start('MenuState');
	};
	
	game.state.add('LoadState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
    var my = parent.MenuState = parent.MenuState || {};
    var game = parent.game;

    var logo;
    var storyButton;
    var playButton;
    var creditsButton;

	my.create = function () {
	    game.stage.backgroundColor = '#ffffff';
	
	    logo = game.add.sprite(
	        game.world.centerX, game.world.centerY - 128, 'title'
	    );
        logo.anchor.setTo(0.5, 0.5);
	
		storyButton = game.add.button(
		    game.world.centerX, game.world.centerY + 64,
		    'storyButton',
		    storyButtonAction,
		    this
		);
		storyButton.anchor.setTo(0.5, 0.5);
		
		playButton = game.add.button(
		    game.world.centerX, game.world.centerY + 168,
		    'gameButton',
		    playButtonAction,
		    this
		);
		playButton.anchor.setTo(0.5, 0.5);
		
		creditsButton = game.add.button(
		    game.camera.width, game.camera.height,
		    'creditsButton',
		    creditsButtonAction,
		    this
		);
		creditsButton.anchor.setTo(1, 1);
	};
	
	function storyButtonAction() {
	    game.state.start('StoryState');
	}
	
	function playButtonAction() {
	    game.state.start('GameState');
	}
	
	function creditsButtonAction() {
	    game.state.start('CreditsState');
	}
	
	game.state.add('MenuState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
	var my = parent.StoryState = parent.StoryState || {};
	var game = parent.game;

	my.create = function () {
	    game.time.events.add(Phaser.Timer.SECOND * 4, fireBuildings, this);
	    game.sound.play('IntroDialog');
	    game.add.sprite(0, 0, 'theMall');
	};
	
	function fireBuildings() {
	    game.time.events.add(Phaser.Timer.SECOND * 6, title2x1, this);
	    game.add.sprite(0, 0, 'fireBuildings');
	}
	
	function title2x1() {
	    game.time.events.add(Phaser.Timer.SECOND * 1, title3x1, this);
	    game.add.sprite(0, 0, 'title2');
	}
	
	function title3x1() {
	    game.time.events.add(Phaser.Timer.SECOND * 1, title2x2, this);
	    game.add.sprite(0, 0, 'title3');
	}
	
	function title2x2() {
	    game.time.events.add(Phaser.Timer.SECOND * 1, title3x2, this);
	    game.add.sprite(0, 0, 'title2');
	}
	
	function title3x2() {
	    game.time.events.add(Phaser.Timer.SECOND * 1, title2x3, this);
	    game.add.sprite(0, 0, 'title3');
	}
	
	function title2x3() {
	    game.time.events.add(Phaser.Timer.SECOND * 1, title3x3, this);
	    game.add.sprite(0, 0, 'title2');
	}
	
	function title3x3() {
	    game.time.events.add(Phaser.Timer.SECOND * 1, finishStory, this);
	    game.add.sprite(0, 0, 'title3');
	}
	
	function finishStory() {
	    game.state.start('MenuState');
	}
	
	game.state.add('StoryState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
	var my = parent.GameState = parent.GameState || {};
	var game = parent.game;

    var WORLD_WIDTH  = 2000;
    var WORLD_HEIGHT = 900;

    // map/level parameters
    var platformList = [
        [550 , 690, 900],
        [660 , 540, 700],
        [550 , 390, 380],
        [1150, 390, 300],
        [850 , 240, 300]
    ];
    
    var laddersList = [
        [700 , 690],
        [1000, 690],
        [1400, 690],
        [900 , 540],
        [1100, 540],
        [750 , 390],
        [1250, 390],
        [900 , 240]
    ];
    
    my.preload = function() {
        // reduce jitter?
        game.renderer.renderSession.roundPixels = true;
        game.stage.smoothed = false;
        
        if (parent.DEBUG) game.time.advancedTiming = true;
    }
  
    var nextWave; 
    var waveNumber; 
    var ground;
    var buildings;
    var platforms;
    var ladders;
    var badguys;
    var player;
    var cursors;
    var jumpButton;
    var attackButton;
    var centerText;
    var leftText;
    var badguyTimer;
    var grayFilter;
    var goodWeapon;
    var defenseItem;
    var moneyItem;
    var background;
    var dog;
    var WAVES = 5;
    var youWon;
    var bossSpawned;
    
    my.create = function() {
        youWon = false;
        bossSpawned = false;
        grayFilter = game.add.filter('Gray');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        // world
        game.world.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT);
        game.stage.backgroundColor = '#85b5e2';
        background = game.add.sprite(0, 0, 'TreeBack');
        background.width = WORLD_WIDTH;
        background.height = WORLD_HEIGHT;
        
        ground = game.add.sprite(0, game.world.height - 60, 'ground');
        game.physics.arcade.enable(ground);
        ground.body.immovable = true;
        
        buildings = game.add.group();
        buildings.enableBody = true;
        addBuilding(buildings, 0, 100);
        addBuilding(buildings, game.world.width - 60, 100);
        
        platforms = game.add.group();
        platforms.enableBody = true;
        for (var i=0; i < platformList.length; i++) {
            var p = platformList[i];
            addPlatform(platforms, p[0], p[1], p[2]);
        }
        
        ladders = game.add.group();
        ladders.enableBody = true;
        for (var i=0; i < laddersList.length; i++) {
            var l = laddersList[i];
            addLadder(ladders, l[0], l[1]);
        }

        goodWeapon = game.add.group();
        goodWeapon.enableBody = true;

        defenseItem = game.add.group();
        defenseItem.enableBody = true;

        moneyItem = game.add.group();
        moneyItem.enableBody = true;
        
        // badguys
        badguys = game.add.group();
        badguys.enableBody = true;
        nextWave = 1;
        waveNumber = 0;
        badguyTimer = game.time.events.add(Phaser.Timer.SECOND * 5, spawnBadGuys, this);
        
        dog =  game.add.sprite(WORLD_WIDTH/2, 0, 'Dog');
        dog.width = 45;
        dog.height = 35;
        game.physics.arcade.enable(dog);
        dog.body.gravity.y = 800;

        // player
        player = game.add.sprite(1000, game.world.height - 260, 'player1');
        //player.height = 50; // use with student asset
        //player.width = 20; // use with student asset
        player.health = 3;
        player.canAttack = true;
        player.weaponType = 'Sword3';
        player.attackV = -450;
        player.money = 30;

        game.physics.arcade.enable(player);
        //player.body.bounce.y = 0.2; // for some reason this causes
                                      // camera jitter when the camera
                                      // is not bounded on at least one side
        player.body.collideWorldBounds = true;
        player.animations.add('left', [0, 1, 2, 3], 10, true);
        player.animations.add('right', [5, 6, 7, 8], 10, true);
        
        // interface layer
        var style = {
            font: '17px "Trebuchet MS", Helvetica, sans-serif',
            fill: '#ffffff',
            boundsAlignH: "center",
            boundsAlignV: "middle"
        };
        
        leftText = game.add.text(0, 0, "Zombie INVISON", style );
        leftText.fixedToCamera = true;
        leftText.cameraOffset.setTo(20, 20);
        
        centerText = game.add.text(0, 0, "", style );
        centerText.fixedToCamera = true;
        //centerText.cameraOffset.setTo(20, 20);
        centerText.setTextBounds(0, 20, game.camera.width, 20);
        
        addGuiPanelItem(4, '1\n$5', 'PoisonTurret', this);
        addGuiPanelItem(5, '2\n$8', 'Basketball', this);
        addGuiPanelItem(6, '3\n$15', 'Tank', this);
        addGuiPanelItem(7, '4\n$20', 'CupcakeOut', this);
        
        // camera
        //
        game.camera.follow(player, 'STYLE_PLATFORMER');
        //game.camera.follow(player, 'STYLE_TOPDOWN_TIGHT');
        
        // controls
        cursors = game.input.keyboard.createCursorKeys();
        jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        attackButton = game.input.keyboard.addKey(Phaser.Keyboard.A);

        poisonTurretButton = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        basketBallButton = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        tankButton = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        cupcakeButton = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);

        poisonTurretButton.onDown.add(function() {
            if (player.money < 5) {
                return;
            }
            player.money -= 5;
            var dir = player.attackV;
            if (dir < 0) {
                dir = -500;
            }
            else {
                dir = 500;
            }
            addDefenseItem(player.x, player.y+player.height-40, 50, 40, 'PoisonTurret', 1, dir,
            function(item) {
                if (!item.coolDown) {
                    s = addGoodWeapon(item.x, item.y-15, 10, 10, item.dir, 0, 'PoisonBullet', 1, 1);
                    game.time.events.add(Phaser.Timer.SECOND * 1,
                        function(ss){ss.destroy();},
                        this, s);
                    item.coolDown = true;
                    game.time.events.add(Phaser.Timer.SECOND * 1.5,
                        function(){item.coolDown = false;},
                        this, item);
                }
            });
        });

        basketBallButton.onDown.add(function() {
            if (player.money < 8) {
                return;
            }
            player.money -= 8;
            var dir = player.attackV;
            if (dir < 0) {
                dir = -250;
            }
            else {
                dir = 250;
            }
            addDefenseItem(player.x, player.y+player.height-30, 30, 30, 'Basketball', 2, dir,
            function(item) {
                if (!item.coolDown) {
                    s = addGoodWeapon(item.x-5, item.y-10, 20, 20, item.dir, 0, 'Fireball', 4, 4);
                    game.time.events.add(Phaser.Timer.SECOND * 1,
                        function(ss){ss.destroy();},
                        this, s);
                    item.coolDown = true;
                    game.time.events.add(Phaser.Timer.SECOND * 3,
                        function(){item.coolDown = false;},
                        this, item);
                }
            });
        });
        
        tankButton.onDown.add(function() {
            if (player.money < 15) {
                return;
            }
            player.money -= 15;
            var dir = player.attackV;
            if (dir < 0) {
                dir = -300;
            }
            else {
                dir = 300;
            }
            addDefenseItem(player.x, player.y+player.height-50, 50, 50, 'Tank', 20, dir,
            function(item) {
                if (!item.coolDown) {
                    game.sound.play('Kablam');
                    s = addGoodWeapon(item.x+10, item.y-20, 25, 25, item.dir, 0, 'PoisonBullet', 6, 6);
                    game.time.events.add(Phaser.Timer.SECOND * 3,
                        function(ss){ss.destroy();},
                        this, s);
                    item.coolDown = true;
                    game.time.events.add(Phaser.Timer.SECOND * 6,
                        function(){item.coolDown = false;},
                        this, item);
                }
            });
        });

        cupcakeButton.onDown.add(function() {
            if (player.money < 20) {
                return;
            }
            player.money -= 20;
            var dir = player.attackV;
            if (dir < 0) {
                dir = -300;
            }
            else {
                dir = 300;
            }
            addDefenseItem(player.x, player.y+player.height-50, 20, 20, 'CupcakeOut', 20, dir,
            function(item) {
                if (!item.coolDown) {
                    game.sound.play('Ppsh');
                    s = addGoodWeapon(item.x-30, item.y-30, 60, 60, 0, 0, 'CupcakeLit', 3, 2000);
                    game.time.events.add(Phaser.Timer.SECOND * 10,
                        function(ss){ss.destroy();},
                        this, s);
                    item.coolDown = true;
                    game.time.events.add(Phaser.Timer.SECOND * 20,
                        function(){item.coolDown = false;},
                        this, item);
                }
            });
        });

        // start audio
        game.sound.play('MoreDefenses');
    }
    
    function spawnBadGuys() {
        // zombies awaken!
        
        var weakBaddies = ['OneEyeGuy', 'GreenGuy'];
        var midBaddies = ['FourEyeGuy'];
        var bossBaddies = ['BigBoss', 'DoubleBoss'];
        waveNumber = nextWave;
        for (var i=0; i < 10*waveNumber; i++) {
            var tough = Math.random() > .8;
            var baddie = weakBaddies[Math.floor(Math.random() * weakBaddies.length)];
            if (tough) {
                baddie = midBaddies[Math.floor(Math.random() * midBaddies.length)];
            }
            var bg = badguys.create(
                Math.floor(
                    80 +
                    (Math.random() * (game.world.width - 160)) -
                    (25 / 4)
                ),
                game.world.height - 60 - 50,
                baddie
            );
            bg.width = 25;
            bg.height = 50;
            bg.body.collideWorldBounds = true;
            bg.health = 1;
            bg.speed = 85;
            if (tough) {
              bg.health += 1;
              bg.speed += 20;
            }
            bg.body.velocity.x = bg.speed;
            bg.climbing = false;
            bg.moneyChance = .8;
            if (Math.random() >= .5) {
                bg.body.velocity.x = -1*bg.speed;
            }
        }

        if (waveNumber != WAVES) {
            nextWave++;
        }
        if (waveNumber % WAVES === 0 && !bossSpawned) {
            bossSpawned = true;
            var theBoss = bossBaddies[Math.floor(Math.random() * bossBaddies.length)];
            var bg = badguys.create(
                70,
                20,
                theBoss
            );
            bg.width = 150;
            bg.height = 300;
            bg.body.collideWorldBounds = true;
            bg.health = 100;
            bg.speed = 150;
            bg.body.velocity.x = bg.speed;
            bg.moneyChance = 20000;
            bg.isBoss = true;
            game.sound.play('Run');
        }
        badguyTimer = game.time.events.add(Phaser.Timer.SECOND * Math.min(55, 30+(waveNumber*3)), spawnBadGuys, this);
    }
    
    my.update = function() {

        var theTime = game.time.now;
        /* start player collison */
        game.physics.arcade.collide(player, ground);
        game.physics.arcade.collide(player, buildings);

        game.physics.arcade.collide(dog, platforms);

        game.physics.arcade.overlap(badguys, goodWeapon,
            function(badguy, weapon) {
                badguy.damage(weapon.power);
                if (badguy.health <= 0) {
                    if (Math.random() < badguy.moneyChance) {
                        if (Math.random() > .6) {
                            game.sound.play('Grrr');
                        }
                        var money = moneyItem.create(badguy.x, badguy.y, 'Dog');
                        money.width = 30;
                        money.height = 30;
                        money.value = 2;
                        if (badguy.isBoss) {
                            money.value = 100;
                        }
                        else {
                            game.time.events.add(Phaser.Timer.SECOND * 12,
                            function(m){m.destroy();},
                            this, money);
                        }
                    }
                    badguy.kill();
                }
                weapon.damage(1);
                if (weapon.health <= 0) {
                    weapon.kill();
                }
            });
        game.physics.arcade.overlap(badguys, defenseItem,
            function(badguy, item) {
                item.damage(1);
                if (item.health <= 0) {
                    item.kill();
                }
            });
        
        //game.physics.arcade.collide(player, platforms);
        for (var i=0; i < platforms.children.length; i++) {
            var p = platforms.children[i];
            
            if (player.y + player.height <= p.y && !cursors.down.isDown) {
                game.physics.arcade.collide(player, p);
            }
        }

        game.physics.arcade.overlap(player, moneyItem,
            function(p, m) {
                p.money += m.value;
                m.kill();
            });
        /* end player collision */
       
        defenseItem.forEachAlive(function(item) {
            item.action(item);
        });

        var bgAlive = false; 
        /* start badguy collision and controller */
        if (badguys.children.length > 0) {
            game.physics.arcade.collide(badguys, ground);
            game.physics.arcade.collide(badguys, buildings,
            function(badguy, building) {
                if (building.x < badguy.x) {
                    badguy.body.velocity.x = badguy.speed;
                }
                else {
                    badguy.body.velocity.x = -1*badguy.speed;
                }
            });
            if (game.physics.arcade.collide(badguys, dog)) {
                game.state.start('LossState');
            }

            game.physics.arcade.overlap(badguys, ladders,
            function(badguy, ladder) {
                if (badguy.isBoss) {
                    return;
                }
                if (!badguy.climbing && badguy.y > ladder.y && Math.random() > .25) {
                    badguy.body.velocity.x = .1*badguy.body.velocity.x;
                    badguy.climbing = true;
                    if (badguy.moneyChance >= .2) {
                        badguy.moneyChance -= .15;
                    }
                }
            });

           
            badguys.forEachAlive(function(bg) {
                bgAlive = true;
                // new ai controller
                bg.body.gravity.y = 800;
                if (bg.isBoss) {
                    return;
                }

                if (intersectsGroup(bg, ladders)) {
                    if (bg.climbing) {
                        bg.body.velocity.y = -.6*bg.speed;
                    }
                }
                else if (bg.climbing) {
                    bg.body.velocity.y = 0;

                    var leftProb = .8;
                    if (bg.x < WORLD_WIDTH/2) {
                        leftProb = .2;
                    }
                    bg.body.velocity.x = bg.speed;
                    if (Math.random() < leftProb) {
                        bg.body.velocity.x = -1*bg.speed;
                    }
                    bg.climbing = false;
                }
                for (var i=0; i < platforms.children.length; i++) {
                    var p = platforms.children[i];

                    if (!bg.climbing) {
                        game.physics.arcade.collide(bg, p);
                    }
                }


            }, this);
            
        }
        if (!bgAlive && bossSpawned) {
           youWon = true;
           waveNumber++;
        }
        /* end badguy collision and controller */
        
        // damage
        game.physics.arcade.overlap(player, badguys, function() {
            if (!player.invincible) {
                game.sound.play('Crunch');
                player.damage(1);
                player.filters = [grayFilter];
                player.invincible = true;
                game.time.events.add(Phaser.Timer.SECOND * 3,
                    function(){player.invincible=false; player.filters = null;},
                    this);
            }
            
        });

        if (player.health <= 0) {
            game.state.start('LossState');
        }

        player.body.gravity.y = 800;
        player.body.velocity.x = 0;
        var canJump = false;
        var canClimb = false;
        
        if (player.body.touching.down) canJump = true;
        if (intersectsGroup(player, ladders)) {
            canJump = true;
            canClimb = true;
        }

        if (attackButton.isDown && player.canAttack) {
            player.canAttack = false;
            s = addGoodWeapon(player.body.center.x, player.body.center.y-7, 15, 30, player.attackV, 0, player.weaponType, 1, 1);
            s.anchor.setTo(.25, .25);
            s.body.angularVelocity = player.attackV;
            game.time.events.add(Phaser.Timer.SECOND * .4,
                    function(){player.canAttack = true;},
                    this, player);

            game.time.events.add(Phaser.Timer.SECOND * .15,
                    function(ss){ss.destroy();},
                    this, s);
        }
        
        if (jumpButton.isDown && canJump) {
            player.body.velocity.y = -450;
        }
        if (canClimb) {  
            if (cursors.up.isDown) {
                player.body.velocity.y = -200;
            }
        }

        if (cursors.left.isDown) {
            //  Move to the left
            player.body.velocity.x = -200;
            player.animations.play('left');
            player.attackV = -450;
        } else if (cursors.right.isDown) {
            //  Move to the right
            player.body.velocity.x = 200;
            player.animations.play('right');
            player.attackV = 450;
        } else {
            //  Stand still
            player.animations.stop();
            player.frame = 4;
        }
    }
    
    my.render = function() {
        centerText.text = 'Next zombie wave in: ' + Math.ceil((badguyTimer.tick - badguyTimer.timer._now) / 1000);
        if (youWon) {
            centerText.text += '\nYou Won!  Congratulations!\nNow test your skills in survival mode';
        }
        if (parent.DEBUG) game.debug.text(game.time.fps || '--', 2, 14, "#00ff00");

        game.debug.text('invincible: '+player.invincible);
        leftText.text = 'Health: '+player.health+'\nMoney: '+player.money;
    }
    
    function addPlatform(group, x, y, width) {
        var p = group.create(x, y, 'platform');
        p.height = 10;
        p.width = width;
        p.body.immovable = true;
        
        return p;
    }
    
    function addBuilding(group, x, y) {
        var b = group.create(x, y, 'building');
        b.height = 740;
        b.width = 60;
        b.body.immovable = true;
        
        return b;
    }
    
    function addLadder(group, x, y) {
        var l = group.create(x, y, 'ladder');
        l.height = 150;
        l.width = 20;
        l.body.immovable = true;
        
        return l;
    }

    function addGoodWeapon(x, y, w, h, xV, yV, resource, power, health) {
        var weapon = goodWeapon.create(x, y, resource);
        weapon.width = w;
        weapon.height = h;
        weapon.body.velocity.x = xV;
        weapon.body.velocity.y = yV;
        weapon.power = power;
        weapon.health = health;
        return weapon;
    }

    function addDefenseItem(x, y, w, h, resource, health, dir, action) {
        var weapon = defenseItem.create(x, y, resource);
        weapon.width = w;
        weapon.height = h;
        weapon.action = action;
        weapon.health = health;
        weapon.dir = dir;
        weapon.anchor.setTo(.5, .5);
        weapon.y += weapon.height/2;
        if (dir < 0) {
            weapon.scale.x *= -1;
        }
        return weapon;
    }
    
    function intersectsGroup(sprite, group) {
        for (var i=0; i < group.children.length; i++) {
            var child = group.children[i];
            if (game.physics.arcade.intersects(sprite, child)) return true;
        }
        
        return false;
    }
    
    function addGuiPanelItem(position, labelText, image, ctx) {
        var x = position * 80 - 70;
        var y = ctx.game.camera.height - 50;
    
        // box
        var box = ctx.game.add.graphics(x, y);
        box.beginFill(0xCCCCCC);
        box.lineStyle(4, 0xFFD900, 0.5);
        box.moveTo(0,0);
        box.lineTo(40, 0);
        box.lineTo(40, 40);
        box.lineTo(0, 40);
        box.lineTo(0, 0);
        box.endFill();
        box.fixedToCamera = true;
        
        // icon
        var icon = ctx.game.add.sprite(x + 5, y + 5, image);
        icon.width = 30;
        icon.height = 30;
        icon.fixedToCamera = true;
        
        // label
        var style = {
            font: '13px "Trebuchet MS", Helvetica, sans-serif',
            fill: '#000000',
            boundsAlignH: "center",
            boundsAlignV: "middle"
        };
        
        var label = ctx.game.add.text(0, 0, labelText, style);
        label.fixedToCamera = true;
        label.cameraOffset.setTo(x + 2, y);
    }
    
	game.state.add('GameState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
    var my = parent.LossState = parent.LossState || {};
    var game = parent.game;

    var restartButton;

	my.create = function () {
		game.world.setBounds(0, 0, game.camera.width, game.camera.height);
                game.stage.backgroundColor = '#ffffff';

		restartButton = game.add.button(
		    game.world.centerX, game.world.centerY,
		    'restartButton',
		    restartButtonAction,
		    this
		);
		restartButton.anchor.setTo(0.5, 0.5);
	};
	
	function restartButtonAction() {
	    game.state.start('MenuState');
	}
	
	game.state.add('LossState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
    var my = parent.CreditsState = parent.CreditsState || {};
    var game = parent.game;

    var creditsText =
        "The awesome team who made this game:\n\n" +
        "Ariyann J, Cherish S, Dashawn B, Isabella R,\n" +
        "John C, Kenney I, Kylee G, Malachi H,\n" +
        "Nathan G, Raul K, Robert M\n"

    var credits;
    var menuButton;

	my.create = function () {
	    game.stage.backgroundColor = '#ffffff';
	    
        var style = {
            font: '17px "Trebuchet MS", Helvetica, sans-serif',
            fill: '#000000',
            boundsAlignH: "center",
            boundsAlignV: "middle"
        };
        
	    credits = game.add.text(0, 0, creditsText, style);
	    credits.setTextBounds(0, 20, game.camera.width, game.camera.height);
	    
		menuButton = game.add.button(
		    game.camera.width, game.camera.height,
		    'menuButton',
		    menuButtonAction,
		    this
		);
		menuButton.anchor.setTo(1, 1);
	};
	
	function menuButtonAction() {
	    game.state.start('MenuState');
	}
	
	game.state.add('CreditsState', my);

	return parent;
}(ZI || {}));

var ZI = (function (parent) {
	var game = parent.game;
    
    game.state.start('BootState');
	
	return parent;
}(ZI || {}));

